require_relative '00_tree_node.rb'

class KnightPathFinder


  attr_reader :move_tree, :starting_pos
  def initialize(pos = [0,0])
    @starting_pos = pos
    # @move_tree = []
    @move_tree = PolyTreeNode.new(starting_pos)
    @visited_positions = [pos]
  end

  def valid_moves(pos)
    valid_moves = []
    start_x, start_y = pos
    poss_x = []
    poss_y = []

    (-2..2).each do |n|
      x2 = start_x + n
      y2 = start_y + n
      poss_x << x2
      poss_y << y2
    end

    poss_x.each do |x|
      poss_y.each do |y|
        if ((start_x - x).abs + (start_y - y).abs == 3)
          valid_moves << [x, y] if x >= 0 && y >= 0
        end
      end
    end

    valid_moves
  end

  def new_move_positions(pos)
    new_moves = valid_moves(pos).reject { |el| @visited_positions.include?(el) }
    # old_moves = valid_moves(pos).select { |el| @visited_positions.include?(el) }
    @visited_positions.concat(new_moves)
    new_moves
  end

  # def build_move_tree(target)
  #   self.move_tree << PolyTreeNode.new(starting_pos)
  #   until move_tree.empty?
  #
  #     current = move_tree.shift
  #     return current if current.value == target
  #     new_move_positions(current.value).each do |node_pos|
  #       new_node = PolyTreeNode.new(node_pos)
  #       new_node.parent = current
  #       # current.children << new_node
  #     end
  #     #@visited_positions << current.value
  #     # debugger
  #     current.children.each do |child|
  #       move_tree.push(child)
  #     end
  #   end
  #   nil
  # end

  def build_move_tree
    current = @move_tree
    possibles = new_move_positions(self.starting_pos)

    possibles.each do |node_pos|
      until possibles.nil?
        child_node = PolyTreeNode.new(node_pos)
        child_node.parent = current
        # current.children << new_node
        possibles.build_move_tree = child.value
      end
    end
    current
  end

  def find_path(end_pos)

  end

end

test_thing = KnightPathFinder.new
p test_thing.build_move_tree
test_thing
