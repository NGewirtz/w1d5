require 'byebug'

class PolyTreeNode

  attr_reader :value
  attr_accessor :children
  def initialize(value = nil)
    @parent = nil
    @children = []
    @value = value
  end

  def parent
    @parent
  end

  def parent=(node)
    # self.parent.children.delete(node)
    if node
      parent.children.delete(self) if parent
      @parent = node
      node.children << self unless node.children.include?(self)
    else
      @parent = nil
    end
  end

  def children
    @children
  end

  def add_child(child)
    @children << child
    child.parent = self
  end

  def remove_child(child)
    raise if child.parent.nil?
    @children.delete(child)
    child.parent = nil
  end

  def dfs(target)
    return self if self.value == target
    children.each do |child|
      result = child.dfs(target)
      next if result.nil?
      return result
    end
    nil
  end

  def bfs(target)
    q = [self]
    until q.empty?

      current = q.shift
      return current if current.value == target

      current.children.each do |child|
        q.push(child)
      end

    end
  end




end
